/*
 * Zadanie 2 Sumy częściowe
 *
 * Rozważ sumy częściowe szeregu definiującego funkcję dzeta Riemanna ζ(s)
 * oraz funkcję eta Dirichleta.
 * Dla s = 2,3.6667,5,7.2,10 oraz n = 50,100,200,500,1000
 * oblicz wartości funkcji ζ(s) i η(s) w pojedynczej precyzji sumując w przód,
 * a następnie wstecz.
 * Porównaj wyniki z rezultatami uzyskanymi dla podwójnej precyzji.
 * Dokonaj interpretacji otrzymanych wyników.
 *
 * Wskazówka
 * Porównaj oszacowania względnych błędów dla działań
 * 	fl(fl(x + y) + z)
 * oraz
 *  fl(x + fl(y + z))
 * przy założeniu, że
 * 	|x + y| < |y + z|.
 */

#include "stdio.h"
#include "math.h"

float  zeta_sp_1toN(float s, int n);
double zeta_dp_1toN(double s, int n);
float  zeta_sp_Nto1(float s, int n);
double zeta_dp_Nto1(double s, int n);

int main(int argc, char **argv)
{
	int precision = 10,
			ns[5] = { 50, 100, 200, 500, 1000 }
			;
	double ss[5] = { 2, 3.6667, 5, 7.2, 10 };

	for (int i = 0; i< 5; i++) {
		for (int j = 0; j< 5; j++) {
			printf("x:= %.4f, n:= % 5d: zeta_sp_1toN := %.*e, zeta_dp_1toN := %.*e\n",
					ss[i], ns[j], zeta_sp_1toN(ss[i], ns[j]), precision,
																  	zeta_dp_1toN(ss[i], ns[j]), precision);

			printf("x:= %.4f, n:= % 5d: zeta_sp_Nto1 := %.*e, zeta_dp_Nto1 := %.*e\n",
					ss[i], ns[j], zeta_sp_Nto1(ss[i], ns[j]), precision,
																		zeta_dp_Nto1(ss[i], ns[j]), precision);
		}
		printf("\n");
	}

}

double zeta_dp_Nto1(double s, int n)
{
	double term = 0,
				result = 0.0;

	while(n > 0) {
		term = 1/pow(n, s);
		/* printf("k := % 3d, term   := % .20f\n", n, term); */
		result += term;
		/* printf("k := % 3d, result := % .20f\n", n, result); */
		n--;
	}
	return result;
};

double zeta_dp_1toN(double s, int n)
{
	double term = 0,
				result = 0.0;
	int k = 1;

	while(k <= n) {
		term = 1/pow(k, s);
		/* printf("k := % 3d, term   := % .20f\n", k, term); */
		result += term;
		/* printf("k := % 3d, result := % .20f\n", k, result); */
		k++;
	}
	return result;
};

float zeta_sp_Nto1(float s, int n)
{
	float term = 0,
				result = 0.0;

	while(n>0) {
		term = 1/pow(n, s);
		/* printf("k := % 3d, term   := % .20f\n", n, term); */
		result += term;
		/* printf("k := % 3d, result := % .20f\n", n, result); */
		n--;
	}
	return result;
};

float zeta_sp_1toN(float s, int n)
{
	float term = 0,
				result = 0.0;
	int k = 1;

	while(k <= n) {
		term = 1/pow(k, s);
		/* printf("k := % 3d, term   := % .20f\n", k, term); */
		result += term;
		/* printf("k := % 3d, result := % .20f\n", k, result); */
		k++;
	}
	return result;
};

/* double eta(double s, int n) */
/* { */
/* 	#<{(| TODO |)}># */
/* 	double term = s, */
/* 				result = 0; */
/* 	int sign; */
/*  */
/* 	int i = 1; */
/* 	double max_error = 1.0 / pow(10, precision); */
/* 	printf("precision := %d, max_error := % 0.*e\n", precision, max_error, precision); */
/* 	while(fabs(term) > max_error) { */
/* 		term = sign * pow(x, 2*i+1)/fact(2*i+1); */
/* 		printf("i := % 3d, term   := % .20f\n", i, term); */
/* 		result += term; */
/* 		printf("i := % 3d, result := % .20f\n", i, result); */
/* 		i++; */
/* 	} */
/* 	return result; */
/* }; */

double fact(double x)
{
	return tgamma(x+1.);
}
