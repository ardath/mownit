/*
 * Zadanie 1 Błędy odejmowania
 * Napisz funkcje, które obliczają wartości poniższych wyrażeń
 * z dokładnością bliską maksymalnej dla wybranej precyzji
 * a) f1(x) = x − sin(x)
 * b) f2(x) = 1 − cos(x)
 */

#include "stdio.h"
#include "math.h"

double f1(double, int);
double fact(double);

int main(int argc, char **argv)
{
	double x = M_PI;
	int precision = 1;
	printf("f1(%.*e, %d) = %.*e\n\n",
			x, precision, precision, f1(x, precision), precision);

	precision = 5;
	printf("f1(%.*e, %d) = %.*e\n\n",
			x, precision, precision, f1(x, precision), precision);

	precision = 10;
	printf("f1(%.*e, %d) = %.*e\n\n",
			x, precision, precision, f1(x, precision), precision);

	precision = 15;
	printf("f1(%.*e, %d) = %.*e\n\n",
			x, precision, precision, f1(x, precision), precision);
}

/*
 * a) f1(x) = x − sin(x)
 * 	 	x - sinx = x - (x - x^3/3! + x^5/5! - x^7/7! + ... )
 * 	  					= x^3/3! - x^5/5! + x^7/7! - ...
 */
double f1(double x, int precision)
{
	double term = x,
				 result = 0;
	int sign;

	int i = 1;
	double max_error = 1.0 / pow(10, precision);
	printf("precision := %d, max_error := % 0.*e\n", precision, max_error, precision);
	while(fabs(term) > max_error) {
		sign = i % 2 == 0 ? -1 : 1;
		term = sign * pow(x, 2*i+1)/fact(2*i+1);
		printf("i := % 3d, term   := % .20f\n", i, term);
		result += term;
		printf("i := % 3d, result := % .20f\n", i, result);
		i++;
	}
	return result;
};

double fact(double x)
{
	return tgamma(x+1.);
}
