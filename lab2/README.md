Lab 2
====

### Zadanie 1 Metoda Gaussa-Jordana
Napisz i sprawdź funkcję rozwiązującą układ równań liniowych n × n metodą Gaussa-Jordana.

Dla rozmiarów macierzy współczynników większych niż 500 × 500 porównaj
czasy działania zaimplementowanej funkcji z czasami uzyskanymi dla linsolve lub
mldivide.

### Zadanie 2 Faktoryzacja LU
Napisz i sprawdź (lu) funkcję dokonującą faktoryzacji A = LU macierzy A.

Zastosuj częściowe poszukiwanie elementu wiodącego.

### Zadanie 3 Analiza obwodu elektrycznego
Napisz program, który:

a) Wczytuje z pliku listę krawędzi grafu opisującego obwód elektryczny. Wagi krawędzi
określają opór fragmentu obwodu między dwoma węzłami.
Wierzchołki grafu identyfikowane są przez liczby naturalne.

b) Wczytuje dodatkowo trójkę liczb (s, t, E), przy czym para (s, t) wskazuje między
którymi węzłami sieci przyłożono siłę elektromotoryczną E. Opór wewnętrzny
SEM można zaniedbać.

c) Wykorzystując prawa Kirchhoffa znajduje natężenia prądu w każdej części obwodu
i przedstawia je na rysunku w postaci grafu ważonego z etykietami.

Rozwiązanie
===

### Zadanie 3

Program wczytuje graf zapisany w pliku CSV.
Przykładowe wywołanie opisano w sekcji
*Uruchamianie*.

Program zapisuje obraz wyjściowego grafu do pliku *circuit_graph.png*.

Dodatkowo generowany jest plik *mst.png* zawierający minimalne drzewo
rozpinające dla wczytanego z pliku CSV grafu.

Nie udało mi się narysować rozwiązania (tj. wartości prądów *na* grafie),
dlatego program wypisuje na standardowe wyjście wartości prądów w cyklach obwodu
i odpowiadające tym cyklom krawędzie.


## Wymagania:
- Ruby >= 2.0
- GraphViz

## Instalacja:

```sh
gem install bundler
bundle install
```

## Uruchamianie:

```sh
# pomoc nt. argumentów programu
bundle exec ./bin/lab2-3.rb -h
# lub
make help

# uruchomienie z przykładowymi argumentami
bundle exec ./bin/lab2-3.rb -s 1 -t 2 -E 100 -f circuit_file.csv
# lub
make run
```

[//]: # (omg this is comment)
[//]: # (![](./circuit_graph.png))
