module Options
  doc = <<-DOCOPT.gsub(/^ {2}/, '').gsub(/] {4}/, ']').gsub(/ {4}\[/, '[')
    #{$PROGRAM_NAME}

    Usage:
    #{$PROGRAM_NAME}\
     -s <node>\
     -t <node>\
    [ -E <value> | --emf=<value> ]\
    [ -f <path> | --file=<path> ]\
    [ -d | --debug ]
    #{$PROGRAM_NAME} -h | --help

    Options:
    -f --file=<path>                    Path to CSV file with circuit definition
    -s <node>                           EMF node 1
    -t <node>                           EMF node 2
    -E --emf=<value>                    EMF value
    -h --help                           Show this screen
    -d --debug                          Show debug info
  DOCOPT

  begin
    @options = Docopt.docopt(doc)
    if @options.fetch('--debug', false) || @options.fetch('-d', false)
      puts "Command line options: "
      puts @options
      $debug = 1
      require 'pry'
    end
  rescue Docopt::Exit => e
    puts e.message
    abort
  end

  def self.get(option)
    @options.fetch(option, false)
  end
end
