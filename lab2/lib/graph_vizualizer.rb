class GraphVizualizer
  def initialize(graph)
    @graph = graph
    @graph_viz = GraphViz.new(:G, type: :graph)
  end

  def draw_png(path)
    add_nodes_and_edges
    create_png(path)
  end

  private

  def create_png(path)
    @graph_viz.output(png: path)
  end

  def add_nodes_and_edges
    @graph.nodes.each {|node|
      @graph_viz.add_nodes(node.to_s)
    }

    @graph.edges.each {|edge|
      @graph_viz.add_edges(*edge.to_graphviz)
    }
  end
end

