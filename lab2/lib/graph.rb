class Graph
  attr_accessor :edges, :nodes

  def initialize
    @nodes = []
    @edges = []
  end

  def each
    edges.each do |edge|
      yield edge
    end
  end

  def add_node(node)
    nodes << node
  end

  def add_edge(from, to, weight)
    edges << Edge.new(from, to, weight)
  end

  def remove_edge(edge)
    edges.delete_if {|e| 
      e.node1 == edge.node1 &&
        e.node2 == edge.node2 &&
        e.weight == edge.weight 
    }
  end
end
