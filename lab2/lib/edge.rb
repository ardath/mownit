class Edge
  attr_accessor :node1, :node2, :weight

  def initialize(node1, node2, weight)
    @node1, @node2, @weight = node1, node2, weight
  end

  def <=>(other)
    self.weight <=> other.weight &&
      self.node1 <=> other.node1 &&
      self.node2 <=> other.node2
  end

  def each
    yield node1
    yield node2
  end

  def has_nodes?(a,b)
    [ a,b ].all? {|node| nodes.include?(node) }
  end

  def nodes
    [ node1, node2 ]
  end

  def label
    [node1, node2].map(&:to_s).join('-')
  end

  def to_s
    "#{node1.to_s} -- #{node2.to_s} (#{weight})"
  end

  def to_graphviz
    [ node1.to_s, node2.to_s, label: weight ]
  end

  def to_a
    [ node1, node2, weight ]
  end
end
