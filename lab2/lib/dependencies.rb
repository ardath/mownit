%W[
  docopt
  graphviz
  options
  graph_csv_reader
  graph_vizualizer
  cycle_finder
  gauss_jordan
  graph
  node
  edge
  kruskal
	pp
].each {|gem|
  begin
    require gem
  rescue LoadError => e
    puts "%s exception while trying to load a '%s' gem/library. " % [ e.class, gem ]
    puts "Message: %s" % [ e.message ]
  end
}
