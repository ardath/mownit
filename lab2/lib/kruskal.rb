require "union_find"

class Kruskal
  def self.compute_mst(graph)
    @graph = graph
    mst = []
    @free_edges = []
    edges = @graph.edges.clone.sort!
    @mst_graph = Graph.new

    union_find = UnionFind.new(@graph.nodes)
    while edges.any? && mst.size <= @graph.nodes.size
      edge = edges.shift
      if !union_find.connected?(edge.node1, edge.node2)
        union_find.union(edge.node1, edge.node2)
        mst << edge
        @mst_graph.add_node(edge.node1) unless @mst_graph.nodes.include?(edge.node1)
        @mst_graph.add_node(edge.node2) unless @mst_graph.nodes.include?(edge.node2)
        @mst_graph.add_edge(edge.node1, edge.node2, edge.weight)
      else
        @free_edges.push edge
      end
    end

    [ @mst_graph, @free_edges ]
  end
end
