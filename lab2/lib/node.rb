class Node
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def <=>(other)
    self.name <=> other.name
  end

  def adjacents(graph)
    graph.edges.select{|e| e.from == self}.map(&:to)
  end

  def to_s
    @name
  end
end
