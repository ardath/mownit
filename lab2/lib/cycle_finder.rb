class CycleFinder
  attr_reader(*[
    :cycles,
  ])
  def initialize(graph)
    @graph = graph
    @cycles = []
  end

  def find_graph_cycles
    @graph.each do |edge|
      edge.each do |node|
        find_cycles(@graph, [node])
      end
    end

    @cycles
  end

  private

  def find_cycles(graph, path)
    start_node = path.first
    sub = []

    #visit each edge and each node of each edge
    graph.each do |edge|
      node1, node2 = edge.nodes
      if edge.nodes.include?(start_node) 
        if node1 == start_node
          next_node = node2
        else
          next_node = node1
        end
      end
      if !visited(next_node, path)
        # neighbor node not on path yet
        sub = [next_node]
        sub.push(*path)
        # explore extended path
        find_cycles(graph, sub)
      elsif path.length > 2  and next_node == path.last
        # cycle found
        p = rotate_to_smallest(path)
        inv = invert(p)
        if is_new(p) && is_new(inv)
          @cycles.push(p)
        end
      end
    end
  end

  def invert(path)
    rotate_to_smallest(path.reverse)
  end

  #  rotate cycle path such that it begins with the smallest node
  def rotate_to_smallest(path)
    n = path.index(path.min)
    path[n..-1]+path[0...n]
  end

  def is_new(path)
    !@cycles.include?(path)
  end

  def visited(node, path)
    path.include?(node)
  end
end
