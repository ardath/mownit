class GraphCSVReader
  attr_reader :delimiter, :nodes, :edges

  def initialize(path, options={})
    @path = path
    @delimiter = options.fetch(:delimiter) { DEFAULT_DELIMITER }
    @nodes = {}
    @edges = {}
    @graph = Graph.new
  end

  def parse_csv_file
    read_graph_file
    parse_lines
    # binding.pry

    @graph
  end

  private

  def parse_lines
    @edges = @lines.each_with_object([]) {|line,edges|
      n1, n2, weight = parse_line(line)

      node1 = find_by_name(n1) || create_and_add_to_graph(n1)
      node2 = find_by_name(n2) || create_and_add_to_graph(n2)

      @graph.add_edge(node1, node2, weight.to_f)
      # edges << edge
    }
  end

  def parse_line(line)
    line.split(delimiter)
  end

  def find_by_name(name)
    if @nodes[name]
      @nodes[name]
    else
      false
    end
  end

  def create_and_add_to_graph(name)
    new_node = Node.new(name)

    @graph.add_node(new_node)
    @nodes[name] = new_node
    new_node
  end

  def read_graph_file
    @lines = File.open(@path, "r") {|f| f.readlines }
      .map(&:chomp)
      .select {|line| line =~ /^\d/ }
  end

  DEFAULT_DELIMITER = ' '
end
