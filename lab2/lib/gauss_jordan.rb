# source adapted from: https://defuse.ca/blog/gaussian-elimination-in-ruby.html
#
# Performs an in-place Gaussian elimination on an NxN matrix 'matrix' (2D array
# of Numeric objects) and an N-element vector 'vector.' (array of N Numerics).
def gaussian_elimination(matrix, vector)
	0.upto(matrix.length - 2) do |pivot_index|
		# ap :iteration #+"#%d" % pivot_index
		# Find the best pivot. This is the one who has the largest absolute value
		# relative to his row (scaled partial pivoting).
		# This step can be omitted to improve speed at the cost of increased error.
		max_rel_value = 0
		max_index = pivot_index
		(pivot_index).upto(matrix.length - 1) do |row_index|
			abs_max_row_element = matrix[row_index].map { |e| e.abs }.max
			rel_value = matrix[row_index][pivot_index] / abs_max_row_element
			# puts "abs_max_row_element: %.2f" % abs_max_row_element
			# puts "matrix[%d][%d]: %.2f" % [ row_index, pivot_index,  matrix[row_index][pivot_index] ]
			# puts "rel_value: %.2f/%.2f ~= %.4f" % [ matrix[row_index][pivot_index], abs_max_row_element, rel_value ]
			if rel_value >= max_rel_value
				max_rel_value = rel_value
				max_index = row_index
			end
		end
		# puts "max_index: %d" % max_index
		# Swap the best pivot row into place.
		matrix[pivot_index], matrix[max_index] = matrix[max_index], matrix[pivot_index]
		vector[pivot_index], vector[max_index] = vector[max_index], vector[pivot_index]
		# puts "matrix after swapping pivot row:"
		# pp matrix

		pivot = matrix[pivot_index][pivot_index]
		# Loop over each row below the pivot row.
		(pivot_index+1).upto(matrix.length - 1) do |row_index|
			# Find factor so that [this row] = [this row] - factor*[pivot row]
			# leaves 0 in the pivot column.
			factor = matrix[row_index][pivot_index] / pivot
			# We know it will be zero.
			matrix[row_index][pivot_index] = 0.0
			# Compute [this row] = [this row] - factor*[pivot row] for the other cols.
			(pivot_index+1).upto(matrix[row_index].length - 1) do |col_index|
				matrix[row_index][col_index] -= factor*matrix[pivot_index][col_index]
			end
			vector[row_index] -= factor * vector[pivot_index]
		end
		# ap "matrix after iteration #%d:" % [pivot_index+1]
		# pp matrix
		# ap "vector after iteration #%d:" % [pivot_index+1]
		# pp vector
		# puts
	end

	return [matrix,vector]
end

# Assumes 'matrix' is in row echelon form.
def back_substitution(matrix, vector)
	(matrix.length - 1).downto( 0 ) do |row_index|
		# ap :back_substitution_iteration
		tail = vector[row_index]
		# puts "vector's tail: %s." % [ tail ]
		(row_index+1).upto(matrix.length - 1) do |col_index|
			tail -= matrix[row_index][col_index] * vector[col_index]
			# puts "col_index: %s;" % [ col_index ]
			# puts "matrix: %s." % [ matrix ]
			matrix[row_index][col_index] = 0.0
		end

		vector[row_index] = tail / matrix[row_index][row_index]
		matrix[row_index][row_index] = 1.0
	end
end

# Example usage:

# A system of equations: matrix * X = vector
# matrix = [
# 	[1.0, 1.0, 1.0, 1.0],
# 	[0.0, 1.0, 2.0, 3.0],
# 	[1.0, 2.0, 4.0, 8.0],
# 	[0.0, 1.0, 4.0, 12.0],
# ]
# vector = [1.0, 0.0, 2.0, 0.0]
# ap "original matrix: "
# pp matrix
# ap "original vector: "
# pp vector
# puts

# Create a backup for verification.
# matrix_backup = Marshal.load(Marshal.dump(matrix))
# vector_backup = vector.dup

# Gaussian elemination to put the system in row echelon form.
# gaussian_elimination(matrix, vector)
# # Back-substitution to solve the system.
# back_substitution(matrix, vector)

# Print the result.
# ap :matrix
# pp matrix
# ap :vector
# pp vector

# # Verify the result.
# pass = true
#
# 0.upto(matrix_backup.length - 1) do  |eqn|
# 	sum = 0
# 	0.upto(matrix_backup[eqn].length - 1) do |term|
# 		sum += matrix_backup[eqn][term] * vector[term]
# 	end
# 	if (sum - vector_backup[eqn]).abs > 0.0000000001
# 		pass = false
# 		break
# 	end
# end
#
# if pass
# 	# ap "Verification PASSED."
# else
# 	# ap "Verification FAILED."
# end
