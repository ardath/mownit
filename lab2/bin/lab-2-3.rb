#!/usr/bin/env ruby

require 'pathname'
$LOAD_PATH.push Pathname($PROGRAM_NAME).dirname.parent.join('lib')

require 'dependencies'
require 'pry'

# parse command line arguments
circuit_file = Options.get('--file')
s = Options.get('-s')
t = Options.get('-t')
$DBG = Options.get('--debug')
EMF = Options.get('--emf').to_f

# read graph from CSV file
graph = GraphCSVReader.new(circuit_file).parse_csv_file

# calculate and returns [Minimum Spanning Tree] and [array of edges not in MST ]
mst_graph, free_edges = Kruskal.compute_mst(graph)

# create PNGs of graph and MST
GraphVizualizer.new(graph).draw_png(circuit_file.gsub('.csv', '.png'))
GraphVizualizer.new(mst_graph).draw_png('mst.png')

# find cycles in graph
graph_cycles = CycleFinder.new(graph).find_graph_cycles
# find cycles in MST
mst_graph_cycles = CycleFinder.new(mst_graph).find_graph_cycles
# MST graph by definition doesn't have cycles
unless mst_graph_cycles.empty?
  abort("Error: MST graph should not contain cycles.")
end

# find nodes in each cycle
cycles_nodes = []
free_edges_copy = free_edges.dup
while free_edges_copy.length > 0
  free_edge = free_edges_copy.pop
  mst_graph.add_edge(*free_edge.to_a)
  cycles = CycleFinder.new(mst_graph).find_graph_cycles

  # binding.pry
  if cycles.length > 1
    abort("Error: MST with one extra edge has more than one cycle.")
  end

  cycle = cycles.pop
  if cycle.length > 3
    next_free_edge = free_edges_copy.pop
    mst_graph.add_edge(*next_free_edge.to_a)
    cycles = CycleFinder.new(mst_graph).find_graph_cycles - [ cycle ]
    # binding.pry
    mst_graph.remove_edge(next_free_edge)
    cycles_nodes.push(*cycles)
  else
    cycles_nodes << cycle
  end
  mst_graph.remove_edge(free_edge)
end

# translate [nodes in cycles] to [edges in cycles]
cycles_edges = []
cycles_nodes.map do |c|
  full_cycle = c << c.first
  cycle_edges = []
  full_cycle.each_cons(2) do |a,b|
		cycle_edges << graph.edges.select {|e| e.has_nodes?(a,b) }
	end
	cycles_edges << cycle_edges.flatten!
end

emf_node_names = [ s, t ]
# checks if edge is an edge with EMF attached.
# compares edges' node names with names of EMF nodes
# given as command line arguments
def emf_attached?(emf_node_names, edge)
	(edge.node1.name == emf_node_names.first && edge.node2.name == emf_node_names.last) ||
	(edge.node2.name == emf_node_names.first && edge.node1.name == emf_node_names.last)
end

# create matrix and vector to be solved with Gauss-Jordan method
# Ax = b
A = []
b = Array.new(cycles_edges.size, 0)
cycles_edges.each_with_index do |cycle,index|
	# store coefficients in array
	currents = Array.new(cycles_edges.size, 0)
	# add weight for every edge in this cycle
	currents[index] += cycle.map(&:weight).inject(0, &:+)
	# subtract weights of other cycles' edges
	cycle.each do |edge|
		cycles_edges.each_with_index do |other_cycle,other_index|
			#	only iterate other cycles
			unless index == other_index
				if other_cycle.include?(edge)
					currents[other_index] -= edge.weight
				end
			end
		end
	end
	A << currents

	# add EMF to vector b
	cycle.map do |edge|
			# puts "edge: %s" % edge
		if emf_attached?(emf_node_names, edge)
			# puts "emf_attached: %s" % edge
			b[index] = EMF
		end
	end
	# binding.pry
end

puts "A: coefficients of equation"
pp  A
puts "b: right side of equation"
pp  b

# solution:
gaussian_elimination(A, b)
back_substitution(A, b)
puts "Solution (rounded to 2 decimal digits):"
pp b.map {|element| element.round(2)}

cycles_edges.each_with_index do |cycle,index|
	puts "Current #%d in solution vector flows through edges: %s" % [ index, cycle.map(&:to_s) ]
end
